package com.colorata.pomogator

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import java.util.*

class ForegroundService : Service() {
    private var isUse = true
    private var localTime = 1500

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action != null && intent.action.equals(ACTION_STOP, ignoreCase = true)) {
            isUse = false
        }
        if (isUse) {
            val notificationChannel = NotificationChannel(
                "1000",
                "Pomogator Timer",
                NotificationManager.IMPORTANCE_NONE
            ).apply {
                description = "Timer for Pomogator"
            }
            val notificationChannelNotify = NotificationChannel(
                "1001",
                "Pomogator Notifier",
                NotificationManager.IMPORTANCE_HIGH
            )
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannels(
                mutableListOf(
                    notificationChannel,
                    notificationChannelNotify
                )
            )

            val notificationRaw = NotificationCompat.Builder(this, "1000")
                .setContentTitle("Timer")
                .setContentText("Working Time!")
                .setSmallIcon(R.drawable.ic_launcher_foreground)

            var notification = notificationRaw
                .setProgress(1500, 1500, false)
                .build()

            startForeground(1, notification)

            timer(1500, onTick = Work@{
                if (isUse) {
                    localTime -= 1
                    notification = notificationRaw
                        .setProgress(1500, localTime, false)
                        .build()
                    notificationManager.notify(1, notification)
                }
                return@Work isUse
            }, onFinish = {
                localTime = 300
                notification = notificationRaw
                    .setContentText("Chilling Time!")
                    .setChannelId("1001")
                    .build()

                notificationManager.notify(2, notification)
                timer(300, onTick = Chill@{
                    if (isUse) {
                        localTime -= 1
                        notification = notificationRaw
                            .setProgress(300, localTime, false)
                            .setContentText("Chilling Time!")
                            .setChannelId("1000")
                            .build()
                        notificationManager.notify(1, notification)
                    }
                    return@Chill isUse
                }, onFinish = {
                    stopSelf()
                })
            })
        } else stopSelf()
        object : CountDownTimer(1000 * 1500, 1000) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {

            }
        }
        return START_STICKY
    }
}

/**
 * Workaround for countDownTimer.
 * @param finishTime time when function will be finished.
 * @param onTick function that will be executed every second. Return true to continue.
 * @param onFinish function that will be executed after **finishTime**.
 * @param localTime from what time to start. Default is 0
 */
fun timer(finishTime: Int, onTick: () -> Boolean, onFinish: () -> Unit, localTime: Int = 0) {
    Timer().schedule(object : TimerTask() {
        override fun run() {
            if (localTime < finishTime) {
                if (onTick()) {
                    timer(finishTime, onTick, onFinish, localTime = localTime + 1)
                }
            } else {
                onFinish()
            }
        }
    }, 1000)
}