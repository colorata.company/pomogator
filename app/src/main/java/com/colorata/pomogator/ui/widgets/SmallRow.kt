package com.colorata.pomogator.ui.widgets

import android.graphics.drawable.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.glance.*
import androidx.glance.action.clickable
import androidx.glance.appwidget.appWidgetBackground
import androidx.glance.appwidget.cornerRadius
import androidx.glance.layout.*
import androidx.glance.text.Text
import androidx.glance.text.TextStyle
import androidx.glance.unit.ColorProvider
import com.colorata.pomogator.appWidgetBackgroundRadius
import com.colorata.pomogator.architecture.Finalizer
import com.colorata.pomogator.architecture.Parameters
import com.colorata.pomogator.drawSlider

@Composable
fun SmallRow() {
    val context = LocalContext.current
    Row(
        modifier = Finalizer.widgetBackground().fillMaxSize().appWidgetBackground().padding(10.dp)
            .appWidgetBackgroundRadius(),
        horizontalAlignment = Alignment.Start,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Finalizer.itemBackground().fillMaxHeight().clickable(Finalizer.switch()),
            contentAlignment = Alignment.Center
        ) {
            Image(
                provider = ImageProvider(
                    Icon.createWithResource(
                        context,
                        Finalizer.icon()
                    ).setTint(Parameters.backgroundColor)
                ),
                contentDescription = null,
                modifier = GlanceModifier.padding(horizontal = 20.dp)
            )
        }
        Text(
            text = "${Parameters.time.toInt() / 60}\n${Parameters.time.toInt() % 60}",
            modifier = GlanceModifier.padding(horizontal = 20.dp),
            style = TextStyle(ColorProvider(Color(Finalizer.colorBar())), fontSize = 25.sp)
        )
        Image(
            provider = ImageProvider(
                bitmap = drawSlider(
                    backgroundColor = Parameters.backgroundColor,
                    foregroundColor = Finalizer.colorBar(),
                    percentage = Parameters.time / Finalizer.slashBar()
                )
            ),
            contentDescription = null,
            modifier = GlanceModifier.height(20.dp)
        )
    }
}