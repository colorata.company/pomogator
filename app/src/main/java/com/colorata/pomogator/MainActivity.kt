package com.colorata.pomogator

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import com.colorata.pomogator.ui.theme.PomogatorTheme

class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            PomogatorTheme {
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Color(androidColor(android.R.attr.textColorSecondary))),
                    color = MaterialTheme.colors.background
                ) {
                    Text(java.lang.String.format("#%06X", 0xFFFFFF and androidColor(android.R.attr.textColorSecondary)))
                }
            }
        }
    }
}